#pragma once

#include "student.h"
#include <string>
using namespace std;

class Group
{
	private:

		string title;		//�������� ������;
		Student* head;		//��������;
		Student** st;		//��� ��������;
		int num;			//���������� ���������;

	public:

		Group(string title);

		Group(Group& gr);

		~Group();

		//�������;
		void setHead(Student* st);

		//�������;
		Student* getHead();
		string getTitle();
		Student* getRandStud();
		friend ostream& operator<<(ostream& os, const Group& group);

		//���������� ��������;
		void addStud(Student* st);

		//����� �������� � ������ �� id;
		Student* findStudId(int id);

		//����� �������� �� �������;
		Student* findStudFio(string fio);

		//�������� �������� �� id;
		int removeStud(int id);

		//��������� ������� ������ �� ������;
		double getAvMark();
};

