#pragma once
#include "Student.h"
#include <iostream>
#include <string>

using namespace std;

class Student;

class Group
{
private:
	int Title;
	Student **St;
	int Num;
	Student *Head;

public:
	Group();
	~Group();
	Group(int title);
	void addSt(Student * St);
	void setHead();
	Student * findST(int id);
	double getAvMark();
	void Delete(int ID);
	int getTitle() { return Title; }
	Student * getHead() { return Head; }
};