#include "Group.h"
#include <string>
#include <time.h>

using namespace std;

Group::Group()
{
	Title = 0;
	St = nullptr;
	Num = 0;
	Head = nullptr;
}

Group::~Group() {}

Group::Group(int title)
{
	Title = title;
	Num = 0;
}

void Group::addSt(Student *stud)
{
	if (Num == 0)
	{
		St = new Student*[Num + 1];
		St[Num] = stud;
	}
	else
	{
		Student **tmp = new Student*[Num + 1];
		for (int i = 0; i < Num; i++)
		{
			tmp[i] = St[i];
		}
		delete[] St;
		St = tmp;
		St[Num] = stud;
	}
	stud->setGroup(this);
	Num++;
}

void Group::setHead()
{
	srand(time(NULL));
	int random = rand() % Num;
	Head = St[random];
	cout << " Head of the Group " << this->Title << "  " << Head->getFio() << endl;
}

Student * Group::findST(int ID)
{
	for (int i = 0; i < Num; i++)
		if (St[i]->getID() == ID)
			return St[i];
	return nullptr;
}

double Group::getAvMark()
{
	if (Num == 0)
		return 0;
	double sum = 0;
	for (int i = 0; i < Num; i++)
	{
		sum += St[i]->getAvMark();
	}
	return (double)sum / Num;
}

void Group::Delete(int ID)
{
	int i;
	for (i = 0; i < Num; i++)
	{
		if (St[i]->getID() == ID)
			break;
	}
	int tmp = i;
	St[tmp] = St[Num];
	Num--;
}