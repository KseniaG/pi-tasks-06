#pragma once
#include "Group.h"
#include "Student.h"
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

using namespace std;

class Dekanat
{
private:
	Student **Students;
	Group **Groups;
	int numS;
	int numG;

public:
	Dekanat();
	~Dekanat();
	void LoadGr();
	void LoadSt();
	void Deduction();
	void AddMark();
	void BestSt();
	void BestGr();
	void AvMarkGr();
	void Head();
	void Transfer();
	void SaveFile();
};