#include "Dekanat.h"
#include <iostream>
#include <string>

using namespace std;

Dekanat::Dekanat()
{
	numS = 0;
	numG = 0;
}

Dekanat::~Dekanat()
{
	for (int i = 0; i < numS; i++)
		delete Students[i];
	delete Students;
	for (int i = 0; i < numG; i++)
		delete Groups[i];
	delete Groups;
}

void Dekanat::LoadGr()
{
	cout << "List of Groups" << endl;
	ifstream file("Group.txt");
	if (!file.is_open())
		cout << "file not found";
	while (!file.eof())
	{
		int Title;
		file >> Title;
		if (numG == 0)
		{
			Groups = new Group*[numG + 1];
			Groups[numG] = new Group(Title);
			cout << Groups[numG]->getTitle() << endl;
		}
		else
		{
			Group **tmp = new Group*[numG + 1];
			for (int i = 0; i < numG; i++)
				tmp[i] = Groups[i];
			delete[] Groups;
			Groups = tmp;
			Groups[numG] = new Group(Title);
			cout << Groups[numG]->getTitle() << endl;
		}
		numG += 1;
	}
	file.close();
}

void Dekanat::LoadSt()
{
	cout << "List of Students" << endl;
	ifstream file("Students.txt");
	if (!file.is_open())
		cout << "file not found";
	while (!file.eof())
	{
		int ID;
		string Fio, f, i, o;
		int Gr;
		file >> ID >> f >> i >> o >> Gr;
		Fio = f + " " + i + " " + o;
		if (numS == 0)
		{
			Students = new Student*[numS + 1];
			Students[numS] = new Student(ID, Fio);
		}
		else
		{
			Student **tmp = new Student*[numS + 1];
			for (int i = 0; i < numS; i++)
				tmp[i] = Students[i];
			delete[]Students;
			Students = tmp;
			Students[numS] = new Student(ID, Fio);
		}
		for (int i = 0; i < numG; i++)
		{
			if (Gr == Groups[i]->getTitle())
				Groups[i]->addSt(Students[numS]);
		}
		cout << Students[numS]->getID() << " " << Students[numS]->getFio() << endl;
		numS++;
	}
	file.close();
}

void Dekanat::Deduction()
{
	for (int i = 0; i < numS; i++)
	{
		if (Students[i]->getAvMark() < 3.0)
		{
			Students[i]->PrintFIO();
			cout << " Expelled " << endl;
			Students[i]->getGroup()->Delete(Students[i]->getID());
			Students[i] = Students[numS - 1];
			numS--;
		}
	}
}

void Dekanat::AddMark()
{
	srand(time(NULL));
	int r;
	for (int i = 0; i < numS; i++)
	{
		r = rand() % 4 + 2;
		cout << Students[numS]->getID() << " " << Students[numS]->getFio() << endl;
		Students[i]->addMarks(r);
	}
}

void Dekanat::BestSt()
{
	double max = 0;
	for (int i = 0; i < numS; i++)
	{
		if (Students[i]->getAvMark() > max)
		{
			max = Students[i]->getAvMark();
		}
		Students[i]->PrintFIO();
		cout << " The best student" << endl;
	}
}

void Dekanat::BestGr()
{
	double max = 0;
	for (int i = 0; i < numG; i++)
	{
		if (Students[i]->getAvMark() > max)
		{
			max = Groups[i]->getAvMark();
		}
		cout << Groups[i]->getTitle() << " The best group" << endl;
	}
}

void Dekanat::AvMarkGr()
{
	for (int i = 0; i < numG; i++)
	{
		cout << " Average mark in the group " << Groups[i]->getTitle() << " " << Groups[i]->getAvMark() << endl;
	}
}

void Dekanat::Head()
{
	for (int i = 0; i < numG; i++)
	{
		Groups[i]->setHead();
	}
}

void Dekanat::Transfer()
{
	int i;
	int ID, Title;
	cout << "Enter the student's ID, which you want to transfer: ";
	cin >> ID;
	cout << "Enter the group, which you want to transfer the student: ";
	cin >> Title;
	for (i = 0; i < numS; i++)
	{
		if (Students[i]->getID() == ID)
		{
			Students[i]->getGroup()->Delete(ID);
			break;
		}
	}
	for (int j = 0; j < numG; j++)
	{
		if (Title == Groups[j]->getTitle())
		{
			Groups[j]->addSt(Students[i]);
		}
	}
	cout << Students[i]->getFio() << " Transferred to the group " << Students[i]->getGroup()->getTitle() << endl;
}

void Dekanat::SaveFile()
{
	ofstream newFile("Info.txt");
	for (int i = 0; i < numS; i++)
	{
		newFile << Students[i]->getID() << "\t" << Students[i]->getFio() << "\tGroup No "
			<< Students[i]->getGroup()->getTitle() << "\t";
		int *mark = Students[i]->getMarks();
		for (int j = 0; j < 10; j++)
		{
			newFile << mark[j];
		}
		newFile << "\t" << Students[i]->getAvMark() << endl << endl;
	}
	newFile.close();
}
