#include "Student.h"
#include "Group.h"
#include <iostream>
#include <string>

using namespace std;

Student::Student()
{
	ID = 0;
	Fio = "";
	Marks = nullptr;
	Num = 0;
}

Student::~Student()
{
	delete[] Marks;
	Marks = nullptr;
}

Student::Student(int ID, string Fio)
{
	this->ID = ID;
	this->Fio = Fio;
	Gr = nullptr;
}

void Student::addMarks(int m)
{
	if (Num == 0)
	{
		Marks = new int[Num + 1];
		Marks[Num] = m;
	}
	else
	{
		int *tmp = new int[Num + 1];
		for (int i = 0; i < Num; i++)
			tmp[i] = Marks[i];
		tmp[Num] = m;
		delete[] Marks;
		Marks = tmp;
	}
	Num++;
}

double Student::getAvMark()
{
	if (Num == 0)
		return 0;
	int sum = 0;
	for (int i = 0; i < Num; i++)
	{
		sum += Marks[i];
	}
	return (double)sum / Num;
}

void Student::PrintMarks()
{
	for (int i = 0; i < Num; i++)
	{
		cout << Marks[i];
	}
	cout << endl;
}