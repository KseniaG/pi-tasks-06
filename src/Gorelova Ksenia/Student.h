#pragma once
#include <iostream>
#include <string>
#include "Group.h"

using namespace std;

class Group;

class Student
{
private:
	int ID;
	string Fio;
	Group * Gr;
	int * Marks;
	int Num;

public:
	Student();
	~Student();
	Student(int ID, string Fio);
	void addMarks(int m);
	double getAvMark();
	void PrintMarks();
	void setGroup(Group * Gr) { this->Gr = Gr; }
	string getFio() { return Fio; }
	void PrintFIO() { cout << Fio; }
	int getID() { return ID; }
	Group * getGroup() { return Gr; }
	Student * getST() { return this; }
	int * getMarks() { return Marks; }
	int getNumM() { return Num; }
};