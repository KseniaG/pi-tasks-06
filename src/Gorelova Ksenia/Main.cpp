#include "Dekanat.h"
#include <string>
#include <iostream>

int main()
{
	setlocale(LC_ALL, "rus");
	Dekanat dek;
	dek.LoadGr();
	dek.LoadSt();
	dek.Deduction();
	dek.AddMark();
	dek.BestSt();
	dek.BestGr();
	dek.AvMarkGr();
	dek.Head();
	dek.Transfer();
	dek.SaveFile();
	return 0;
}